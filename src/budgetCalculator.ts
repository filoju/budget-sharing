class BudgetCalculator {
    calculateTotalExpense(expenses: number[]): number {
        return expenses.reduce((acc, expense) => acc + expense, 0);
    }

    calculateShare(expenses: number[], totalExpense: number): number[] {
        const numFriends = expenses.length;
        const individualShare = totalExpense / numFriends;
        return expenses.map(expense => individualShare - expense);
    }
}

export default BudgetCalculator;
