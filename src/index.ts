import BudgetCalculator from './budgetCalculator';

const expenses: number[] = [50, 30, 20];
const calculator = new BudgetCalculator();
const totalExpense = calculator.calculateTotalExpense(expenses);
const shares = calculator.calculateShare(expenses, totalExpense);

console.log('Total Expense:', totalExpense);
console.log('Individual Shares:', shares);
