class Expense {
    constructor(public description: string, public amount: number, public payer: string) { }
}

class ExpenseGroup {
    expenses: Expense[] = [];

    addExpense(expense: Expense) {
        this.expenses.push(expense);
    }


    closeGroup(): { totalExpense: number, perPerson: number, balances: Record<string, number> } {
        const totalExpense = this.expenses.reduce((acc, expense) => acc + expense.amount, 0);
        const perPerson = totalExpense / this.expenses.length;
        const balances: Record<string, number> = {};

        this.expenses.forEach(expense => {
            if (!balances[expense.payer]) {
                balances[expense.payer] = 0;
            }
            balances[expense.payer] += perPerson - expense.amount;
        });

        return { totalExpense, perPerson, balances };
    }
}

const group = new ExpenseGroup();
group.addExpense(new Expense('Billet de Train', 25, 'Marc'));
group.addExpense(new Expense('Sac de pomme pour le pique-nique', 2.99, 'Hamza'));


const closedGroup = group.closeGroup();
console.log('Total Expense:', closedGroup.totalExpense);
console.log('Total per Person:', closedGroup.perPerson);
console.log('Balances:', closedGroup.balances);
